dataList = []
statsFile = open('../data/cemeteryStats.txt', 'w')
with open('../data/cemeteryData.csv') as f:
    for line in f:
        subList = []
        fields = line.split(',')
   
        if not fields[3].isdigit():
            continue
        try:
            int(fields[2][-4:])
        except:
            continue
        subList.append(fields[2][-4:])
        subList.append(int(fields[3]))
        subList.append(fields[4])
        dataList.append(subList)



pre30 = []
post30 = []
pre30M = []
pre30F = []
post30M = []
post30F = []
totalA = []
totalM = []
totalF = []


for entry in dataList:
    totalA.append(entry[1])
    if entry[2][0] == 'M':
        totalM.append(entry[1])
    if entry[2][0] == 'F':
        totalF.append(entry[1])

    if int(entry[0]) < 1930:
        pre30.append(entry[1])
        if entry[2][0] == 'M':
            pre30M.append(entry[1])
        if entry[2][0] == 'F':
            pre30F.append(entry[1])
    else:
        post30.append(entry[1])
        if entry[2][0] == 'M':
            post30M.append(entry[1])
        if entry[2][0] == 'F':
            post30F.append(entry[1])

total = 0
for i in totalA:
    total += i
print("Total number of people: ", len(totalA), file=statsFile)
print("Mean age of all people: %.2f" % (total/len(totalA)), file=statsFile)
print("Median age of all people: ", sorted(totalA)[len(totalA)//2], file=statsFile)
print(file=statsFile)

total = 0
for i in totalM:
    total += i
print("Total number number of males: ", len(totalM), file=statsFile)
print("Males mean age: %.2f"% (total/len(totalM)), file=statsFile)
print("Males median age: ", sorted(totalM)[len(totalM)//2], file=statsFile)
print(file=statsFile)

total = 0
for i in totalF:
    total += i
print("Total number of females: ", len(totalF), file=statsFile)
print("Female mean age: %.2f"% (total/len(totalF)), file=statsFile)
print("Female median age: ", sorted(totalF)[len(totalF)//2], file=statsFile)
print(file=statsFile)


total = 0
for i in pre30:
    total += i
print("Total number of people who died before 1930: ", len(pre30), file=statsFile)
print("Pre 1930 mean age: %.2f"% (total/len(pre30)), file=statsFile)
print("Pre 1930 median age: ", sorted(pre30)[len(pre30)//2], file=statsFile)
print(file=statsFile)

total = 0
for i in pre30M:
    total += i
print("Number of males who died before 1930: ", len(pre30M), file=statsFile)
print("Pre 1930 male mean age: %.2f"% (total/len(pre30M)), file=statsFile)
print("Pre 1930 male median age: ", sorted(pre30M)[len(pre30M)//2], file=statsFile)
print(file=statsFile)

total = 0
for i in pre30F:
    total += i
print("Number of females who died before 1930: ", len(pre30F), file=statsFile)
print("Pre 1930 female mean age: %.2f"% (total/len(pre30F)), file=statsFile)
print("Pre 1930 female median age: ", sorted(pre30F)[len(pre30F)//2], file=statsFile)
print(file=statsFile)

total = 0
for i in post30:
    total += i
print("Total number of people who died after 1930: ", len(post30), file=statsFile)
print("Post 1930 mean age: %.2f"% (total/len(post30)), file=statsFile)
print("Post 1930 median age: ", sorted(post30)[len(post30)//2], file=statsFile)
print(file=statsFile)

total = 0
for i in post30M:
    total += i
print("Number of males who died after 1930: ", len(post30M), file=statsFile)
print("Post 1930 male mean age: %.2f"% (total/len(post30M)), file=statsFile)
print("Post 1930 male median age: ", sorted(post30M)[len(post30M)//2], file=statsFile)
print(file=statsFile)

total = 0
for i in post30F:
    total += i
print("Number of females who died after 1930: ", len(post30F), file=statsFile)
print("Post 1930 female mean age: %.2f"% (total/len(post30F)), file=statsFile)
print("Post 1930 female median age: ", sorted(post30F)[len(post30F)//2], file=statsFile)
print(file=statsFile)

for i in range(186, 202):
    total = 0
    num = 0
    totalM = 0
    numM = 0
    totalF = 0
    numF = 0

    for entry in dataList:
        if entry[0][-4:-1] == str(i):
            total += entry[1]
            num += 1
            if entry[2][0] == 'M':
                totalM += entry[1]
                numM += 1
            if entry[2][0] == 'F':
                totalF += entry[1]
                numF += 1
    if num > 0:
        print(str(i) + "0's total number of people: ", num, file=statsFile)
        print(str(i) + "0's total mean age: %.2f\n"% (total/num), file=statsFile)
    if numM > 0:
        print(str(i) + "0's total number of males: ", numM, file=statsFile)
        print(str(i) + "0's male mean age: %.2f\n"% (totalM/numM), file=statsFile)
    if numF > 0:
        print(str(i) + "0's total number of females: ", numF, file=statsFile)
        print(str(i) + "0's female mean age: %.2f"% (totalF/numF), file=statsFile)
    print("\n", file=statsFile)
statsFile.close()
