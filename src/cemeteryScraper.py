from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from bs4 import BeautifulSoup
import requests

maleNames = dict()
femaleNames = dict()
with open('../data/names.txt') as names:
    for line in names:
        words = line.split()
        maleNames[words[1].strip().lower()] = int(words[0].strip())
        femaleNames[words[2].strip().lower()] = int(words[0].strip())
names.close()

csvFile = open('../data/cemeteryData.csv', 'w')
driver = webdriver.Firefox()
driver.get("http://namesinstone.com/Search.aspx?cemeteryId=106")

for i in range(1987):
    print("Working on page: ",i)
    soup_index = BeautifulSoup(driver.page_source, 'html.parser')
    links = soup_index.find_all('a')
    for link in links:
        try:
            if link.contents[0] == 'Deceased Page':
                deceasedPage = 'http://namesinstone.com/' + link.get('href')
                deceasedPageText = requests.get(deceasedPage)
                soup_page = BeautifulSoup(deceasedPageText.text, 'html.parser')
                name = soup_page.find('title').contents[0].split(' -')[0].strip()
                print(name + ',', end='', file=csvFile)
                birthdate = soup_page.find(id="ctl00_cphMain_dbxCemetery_gdcBirthDate_tdPlaceView").contents[0].strip()
                print(birthdate + ',', end='', file=csvFile)
                deathdate = soup_page.find(id="ctl00_cphMain_dbxCemetery_gdcDeathDate_tdPlaceView").contents[0].strip()
                print(deathdate + ',', end='', file=csvFile)

                try:
                    age = int(deathdate[-4:]) - int(birthdate[-4:])
                    print(str(age) + ',', end='', file=csvFile)
                except:
                    print(' ,', end='', file=csvFile) 

                firstName = name.split()[0].lower()
                if firstName not in maleNames and firstName not in femaleNames:
                    print("U", file=csvFile)
                elif firstName in maleNames and firstName not in femaleNames:
                    print("M", file=csvFile)
                elif firstName not in maleNames and firstName in femaleNames:
                    print("F", file=csvFile)
                else:
                    if maleNames[firstName] < femaleNames[firstName]:
                        print("M", file=csvFile)
                    else:
                        print("F", file=csvFile)


        except:
            continue
    nextPage_button = driver.find_element_by_id("ctl00_cphMain_pagingTop_linkNext")
    nextPage_button.click()
csvFile.close()
driver.quit()
